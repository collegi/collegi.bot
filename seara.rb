require 'rubygems'
require 'discordrb'
require 'httparty'
require 'wit'
require 'json'
require 'yaml'
require 'yaml/store'
require 'securerandom'
require './lib/minecraft-query'
require './lib/utilities/chat_utilities'
require './lib/utilities/pre_filter'
require './modules/mention_handler'
require './modules/message_handler'
require './modules/command_handler'
require './modules/discord_handler'

# Drop our configuration file into a constant.
CONFIG      = YAML.load_file('config/seara.yaml')

# Load the YAML stores that Seara uses. The first one is to store the
# conversation states used by the cleverbot API, and the second contains all
# automated moderation actions that she takes per programming.
CONVERSE_DB = YAML::Store.new('databases/conversations.yaml')
AUTOACT_DB  = YAML::Store.new('databases/automatedbans.yaml')

# Establish the wit.ai connection needed for the "Programmed" core.
WITAI = Wit.new(access_token: CONFIG['apis']['programmed_core']['wit_token'])

# Establish the DiscordRB Bot (Seara)
SEARA = Discordrb::Commands::CommandBot.new               \
  token:      CONFIG['apis']['discord']['secret_token'],  \
  client_id:  CONFIG['apis']['discord']['client_id'],     \
  prefix:     '!', parse_self: true, advanced_functionality: true

# Establish our two ways to talk to the minecraft server. RCON allows for us
# to send commands to minecraft, QUERY lets us get information from the server.
# Final line initializes the RCON connection with the password from CONFIG.
MC_RCON = RCON::Minecraft.new(      \
  CONFIG['minecraft']['server_ip'], \
  CONFIG['minecraft']['rcon_port']  \
)

MC_QUERY = Query.simpleQuery(       \
  CONFIG['minecraft']['server_ip'], \
  CONFIG['minecraft']['query_port'] \
)

MC_RCON.auth(CONFIG['minecraft']['rcon_password'])

# Extend Seara with CommandContainers specified in the modules directory.
# You can refer to those files for more information on what each module does.
SEARA.include! MessageHandler
SEARA.include! MentionHandler
SEARA.include! DiscordHandler
SEARA.include! CommandHandler

# Enable Seara
SEARA.run
