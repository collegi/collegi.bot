# Commands that are limited to privileged users only
module StaffCommands
  include ChatUtils

  # HACK: Fix this fucking disaster
  def self.cmd_broadcast(event)
    message = event.content.strip.split(' ')
    message.delete_at(1)
    message.delete_at(0)
    message = message.join(' ')
    command = CONFIG['minecraft']['commands']['broadcast']
    command = command + ' ' + message
    MC_RCON.command(command)
  end

  # Spit out an error message.
  def self.cmd_error(event)
    SEARA.send_message(                             \
      event.channel.id,                             \
      CONFIG['messages']['staff_commands']['error'] \
    )
  end

  # Spit out help message.
  def self.cmd_help(event)
    event.message.delete
    CONFIG['messages']['staff_commands']['help'].each do |_key, value|
      ChatUtils.temporary(event, value)
    end
  end

  # Process the command message.
  def self.process(event)
    return unless CONFIG['bot']['privileged-users'].include?(event.user.id)
    command = event.content.strip.split(' ')
    case command[1]
    when 'broadcast'
      cmd_broadcast(event)
    when 'help'
      cmd_help(event)
    else
      cmd_error(event)
    end
  end
end
