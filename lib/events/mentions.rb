module Mentions
  include ChatUtils

  # Section of the script to handle unhandled mentions. This used to handle any
  # message that mentioned the bots name but we don't do that anymore. Nope.
  def self.converse_core(event)
    # Build the beginning of the API call
    url = 'https://www.cleverbot.com/getreply'
    url =  url + '?key='   + CONFIG['apis']['converse_core']['clever_key']
    url =  url + '&input=' + event.message.content.gsub(/\@\w+/, "").gsub(/\<|\>/, "")
    # Let's check if this user has a conversation with seara on this server.
    # First we need to check if we're even on a server
    if event.server.nil?
      user_id = event.message.author.id.to_s
    else
      user_id = event.server.id.to_s + '-' + event.message.author.id.to_s
    end
    conv_id = CONVERSE_DB.transaction { CONVERSE_DB.fetch(user_id, nil) }
    # Process accordingly - If no conv id exists, generate one, otherwise build
    # url and process accordingly.
    if conv_id.nil?
      url      = URI.escape(url)
      response = JSON.parse(HTTParty.get(url).to_json)
      # Store the new Conversation ID in the database for future use
      CONVERSE_DB.transaction do
        CONVERSE_DB[user_id]        = response['cs']
        CONVERSE_DB[:last_modification] = Time.now
      end
    else
      url = url + '&cs=' + conv_id
      url = URI.escape(url)
      response = JSON.parse(HTTParty.get(url).to_json)
      # Get the new conversation state code and save it
      CONVERSE_DB.transaction do
        CONVERSE_DB[user_id]            = response['cs']
        CONVERSE_DB[:last_modification] = Time.now
      end
    end
    ChatUtils.responder(event, response['output'])
  end

  def self.process(event)
    ai_says = WITAI.converse(SecureRandom.uuid, "#{event.message.content}", {})
    if ai_says['confidence'] > 0.70
      ChatUtils.responder(event, ai_says['msg'])
    else
      converse_core(event)
    end
  end
end
