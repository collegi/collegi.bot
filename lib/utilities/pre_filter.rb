module PreFilter

  # The check function. This sees if the message contains anything we are
  # filtering out and flags the information accordingly.
  def self.check_message(event)
    if event.message.content.match(     \
        Regexp.new(CONFIG['prefilter']['level_0'], "i"))
      event_0(event)
      return 0
    elsif event.message.content.match(  \
            Regexp.new(CONFIG['prefilter']['level_1'], "i"))
      event_1(event)
      return 1
    elsif event.message.content.match(  \
            Regexp.new(CONFIG['prefilter']['level_2'], "i"))
      #event_2(event)
      return 2
    elsif event.message.content.match(  \
            Regexp.new(CONFIG['prefilter']['level_3'], "i"))
      #event_3(event)
      return 3
    else
      return 9
    end
  end

  def self.discord_ban(event, user, code)
    message = 'You have been banned from this server because your username '  \
            + 'matches that of a user on minecraft that utilized a level 0 '  \
            + 'offensive term, or because you used a level 0 offensive term ' \
            + 'in the discord chat. If you believe this to be in error,'      \
            + 'please appeal. Your appeal code is: ' + code.to_s + '.'
    user.pm(message)
    event.server.ban(user)
  end

  def self.discord_kick(event, user, code)
    message = 'You have been kicked from this server because your username '  \
            + 'matches that of a user on minecraft that utilized a level 1 '  \
            + 'offensive term, or because you used a level 1 offensive term ' \
            + 'in the discord chat. While it is possible for you to rejoin, ' \
            + 'please pay attention to your word usage in the future. The '   \
            + 'automated action code for this event was: ' + code.to_s        \
            + '. Please use this code to appeal if you feel this was in error.'
    user.pm(message)
    event.server.kick(user)
  end

  def self.discord_search(username)
    SEARA.users.each do |id, user|
      return user if user.name.eql?(username)
    end
  end

  def self.event0_m2d(event, username, action_code)
    MC_RCON.command(CONFIG['minecraft']['commands']['chatclear'])
    mc_ban(username, action_code)
    discord_user = discord_search(username)
    discord_ban(event, discord_user, action_code) unless discord_user.nil?
  end

  def self.event0_d2m(event, username, action_code)
    MC_RCON.command(CONFIG['minecraft']['commands']['chatclear'])
    mc_ban(username, action_code)
    discord_ban(event, event.message.author, action_code)
  end

  def self.event0_donly(event, username, action_code)
    mc_ban(username, action_code)
    discord_ban(event, event.message.author, action_code)
  end

  def self.event_0(event)
    action_code = SecureRandom.uuid
    username    = get_username(event)
    return 0 if username.eql?('Seara')
    if event.channel.id.to_s.eql?(CONFIG['channels']['spongebridge_chat'])
      event0_m2d(event, username, action_code) if event.message.author.current_bot?
      event0_d2m(event, username, action_code) unless event.message.author.current_bot?
    else
      event0_donly(event, username, action_code)
    end
    delete_and_log(event, username, 'Level 0 - Immediate Ban', action_code)
  end

  def self.event1_m2d(event, username, action_code)
    MC_RCON.command(CONFIG['minecraft']['commands']['chatclear'])
    mc_tempban(username, action_code)
    discord_user = discord_search(username)
    discord_kick(event, discord_user, action_code) unless discord_user.nil?
  end

  def self.event1_d2m(event, username, action_code)
    MC_RCON.command(CONFIG['minecraft']['commands']['chatclear'])
    mc_tempban(username, action_code)
    discord_kick(event, event.message.author, action_code)
  end

  def self.event1_donly(event, username, action_code)
    mc_tempban(username, action_code)
    discord_kick(event, event.message.author, action_code)
  end

  def self.delete_and_log(event, username, match_type, action_code)
    event.message.delete
    log_actions(event, username, match_type, action_code)
  end

  def self.event_1(event)
    action_code = SecureRandom.uuid
    username    = get_username(event)
    return 0 if username.eql?('Seara')
    if event.channel.id.to_s.eql?(CONFIG['channels']['spongebridge_chat'])
      event1_m2d(event, username, action_code) if event.message.author.current_bot?
      event1_d2m(event, username, action_code) unless event.message.author.current_bot?
    else
      event1_donly(event, username, action_code)
    end
    delete_and_log(event, username, 'Level 1 - Temporary Ban', action_code)
  end

  def self.event_2(event)
    action_code = SecureRandom.uuid
    username    = get_username(event)
    return 0 if username.eql?('Seara')
    log_actions(event, username, 'Level 2 - Warnable Offense', action_code)
  end

  def self.event_3(event)
    action_code = SecureRandom.uuid
    username    = get_username(event)
    return 0 if username.eql?('Seara')
    log_actions(event, username, 'Level 3 - Tracking', action_code)
  end

  def self.get_username(event)
    if event.message.author.current_bot? \
        && event.channel.id.to_s.eql?(CONFIG['channels']['spongebridge_chat'])
      content = event.message.content.split('>')
      content[0].delete('`').delete(' ')
    else
      event.message.author.name
    end
  end

  def self.log_actions(event, username, match_type, code)
    log_channel(username, match_type, code)
    log_console(event, username, match_type, code)
    log_database(event, username, match_type, code)
  end

  def self.log_channel(username, match_type, code)
    message = Time.now.getutc.strftime('%Y-%m-%d %H:%M:%S %z:')               \
              + ' A message was detected that triggered the PreFilter. The '  \
              + 'message was a '  + match_type  + ' match. The username '     \
              + 'involved was '   + username    + '. More information can be '\
              + 'found under Automated Action Code: ' + code
    SEARA.send_message(CONFIG['channels']['bot_monitoring'], message)
  end

  def self.log_console(event, username, match_type, code)
    puts '----- ALERT: Message has Matched PreFilter -----'
    puts 'Match Type: ' + match_type
    puts 'Server: '     + event.server.name   + ' - ' + event.server.id.to_s
    puts 'Channel: '    + event.channel.name  + ' - ' + event.channel.id.to_s
    puts 'User: '       + username
    puts 'Message Content: '                  + event.message.content
    puts 'Automated Action Code: '            + code
    puts 'Taking Appropriate Action...'
    puts '---------------------------------------------'
  end

  def self.log_database(event, username, match_type, code)
    data = Time.now.to_s  + ' - ' + match_type + ' - ' + username
    data = data           + ' - ' + event.message.content
    AUTOACT_DB.transaction do
      AUTOACT_DB[code]               = data
      AUTOACT_DB[:last_modification] = Time.now
    end
  end

  def self.mc_ban(username, code)
    command = CONFIG['minecraft']['commands']['ban']
    command = command + ' ' + username
    command = command + ' ' + 'Automated Ban: You have issued a Level 0'
    command = command + ' ' + 'offensive word. Appeal Code: ' + code
    MC_RCON.command(command)
  end

  def self.mc_tempban(username, code)
    command = CONFIG['minecraft']['commands']['tempban']
    command = command + ' ' + username + ' ' + '24h'
    command = command + ' ' + 'Automated 24H Ban: You have issued a Level 1'
    command = command + ' ' + 'offensive word. Appeal Code: ' + code
    MC_RCON.command(command)
  end

  # def self.mc_warn(username, text, code)
  #  MC_RCON.command(command)
  # end
end
