# Extend the DiscordRB event container for discord specific events.
module DiscordHandler
  extend Discordrb::EventContainer

  # Detect when Seara has reached the ready condition.
  ready do |event|
    game = SecureRandom.random_number 5
    case game
    when 0
      SEARA.game = 'Minecraft'
    when 1
      SEARA.game = 'Overwatch'
    when 2
      SEARA.game = 'World of Warcraft'
    when 3
      SEARA.game = 'Portal 2'
    when 4
      SEARA.game = 'DanceDanceRevolution'
    when 5
      SEARA.game = "with Cia's heart."
    end
    ChatUtils.logging(event, 'startup')
  end

  # Detect when a channel is created.
  channel_create do |event|
    ChatUtils.logging(event, 'channel_create')
  end

  # Detect when a channel is deleted.
  channel_delete do |event|
    ChatUtils.logging(event, 'channel_delete')
  end
end
